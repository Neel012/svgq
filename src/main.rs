/// xml
/// svgq
/// https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths
use std::fs::File;
use std::path::Path;
use std::path::PathBuf;
use std::io::prelude::*;

fn svg_template(elipse_width: &str) -> String {
  // br#"
  format!(r#"
<svg
   width="210mm"
   height="297mm"
   viewBox="0 0 210 297"
   docname="drawing.svg">
  <g id="layer1">
    <ellipse
       id="path3713"
       cx="50"
       cy="104.23214"
       rx="{}"
       ry="40.82143"
       style="stroke-width:0.26458332;fill:#0000ff" />
    <path
       style="fill:none;stroke:#008000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="m 56.696427,214.60119 c 47.625003,32.50595 111.125003,6.04762 116.416663,-4.53572 5.29167,-10.58333 21.92262,-101.29762 18.14286,-108.10119 -3.77976,-6.803567 -8.31548,-55.184519 -8.31548,-55.184519"
       id="path3717"
        />
  </g>
</svg>
  "#, elipse_width)
}

/*
<svg width="190" height="160" xmlns="http://www.w3.org/2000/svg">

  <path d="M 10 10 C 20 20, 40 20, 50 10" stroke="black" fill="transparent"/>
  <path d="M 70 10 C 70 20, 120 20, 120 10" stroke="black" fill="transparent"/>
  <path d="M 130 10 C 120 20, 180 20, 170 10" stroke="black" fill="transparent"/>
  <path d="M 10 60 C 20 80, 40 80, 50 60" stroke="black" fill="transparent"/>
  <path d="M 70 60 C 70 80, 110 80, 110 60" stroke="black" fill="transparent"/>
  <path d="M 130 60 C 120 80, 180 80, 170 60" stroke="black" fill="transparent"/>
  <path d="M 10 110 C 20 140, 40 140, 50 110" stroke="black" fill="transparent"/>
  <path d="M 70 110 C 70 140, 110 140, 110 110" stroke="black" fill="transparent"/>
  <path d="M 130 110 C 120 140, 180 140, 170 110" stroke="black" fill="transparent"/>

</svg>
*/

/*
<svg width="190" height="160" xmlns="http://www.w3.org/2000/svg">
  <path d="M 10 80 C 40 10, 65 10, 95 80 S 150 150, 180 80" stroke="black" fill="transparent"/>
</svg>
*/

struct Svg;

impl Svg {
  fn new(width: u32, height: u32) -> Svg {
    print!(r#"
    <svg
       width="{width}mm"
       height="{height}mm"
       viewBox="0 0 {width} {height}"
    "#, width = width, height = height);
    Svg {}
  }

  fn close(&self) {
    print!("</svg>")
  }

  fn path(&self) {
    r#"
    <path
       style="fill:none;stroke:#008000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="m 56.696427,214.60119 c 47.625003,32.50595 111.125003,6.04762 116.416663,-4.53572 5.29167,-10.58333 21.92262,-101.29762 18.14286,-108.10119 -3.77976,-6.803567 -8.31548,-55.184519 -8.31548,-55.184519"
       id="path3717"
        />
    "#;
  }
}

struct SvgPath;

impl SvgPath {
  fn new() -> SvgPath {
    r#"
    <path
       style="fill:none;stroke:#008000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="m 56.696427,214.60119 c 47.625003,32.50595 111.125003,6.04762 116.416663,-4.53572 5.29167,-10.58333 21.92262,-101.29762 18.14286,-108.10119 -3.77976,-6.803567 -8.31548,-55.184519 -8.31548,-55.184519"
       id="path3717"
        />
    "#;
    SvgPath
  }

  /// c1 first control point
  /// c2 second control point
  fn curveto(&self, c1: (f32, f32), c2: (f32, f32), dest: (f32, f32)) {

  }
}

fn main() {
  // std::env::args().nth(1).map(|filepath| {
    // let mut file = File::create(filepath).unwrap();
    // file.write_all(svg_template()).unwrap();
  // });
  let points: Vec<_> = std::env::args().skip(1).collect();
  // for x in points {
  //   println!("{}", x);
  // }
  // print!("{}", svg_template(&points[0]));
  Svg::new(100, 120);
}
